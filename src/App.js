import React from 'react';
import { createUseStyles } from 'react-jss';
import DataTable from './components/DataTable';

const useStyles = createUseStyles({
  app: {
    background: '#d4d4d4',
    height: '100vh',
  },
});

function App() {
  const classes = useStyles();

  return (
    <div className={classes.app}>
      <div className="container">
        <DataTable />
      </div>
    </div>
  );
}
export default App;
