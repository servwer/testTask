import React from 'react';
import { createUseStyles } from 'react-jss';
import { Form, Field } from 'react-final-form';
import axios from 'axios';

const useStyles = createUseStyles({
  form: {
    display: 'grid',
    '& div': {
      marginTop: 20,
      '& h3': {
        fontWeight: 400,
      },
      '& $button': {
        marginTop: 0,
      },
    },
  },
  input: {
    padding: 10,
    fontSize: 16,
    outline: 0,
    color: '#313131',
  },
  subtitle: {
    fontSize: 24,
    color: '#313131',
  },
  button: {
    fontSize: 20,
    outline: 0,
    padding: '10px 0px',
    cursor: 'pointer',
    width: '100%',
  },
  note_add: {},
});

function AddNote({ addNote, updateList }) {
  const classes = useStyles();
  const [currentId, setCurrentId] = React.useState('');

  function onSubmit(value) {
    addNote(value);
    setCurrentId(value._id);
    setTimeout(() => {
      updateList();
    }, 200);
    axios.put('http://178.128.196.163:3000/api/records', {
      data: {
        name: value.name,
        email: value.email,
        age: value.age,
        phone: value.phone,
      },
    }).then(() => updateList)
      .catch((err) => console.log(err));
  }

  return (
    <div className={classes.note_add}>
      <h2 className={classes.subtitle}>Добавить запись</h2>
      <Form
        onSubmit={onSubmit}
        render={({
          handleSubmit, form, submitting,
        }) => (
          <form className={classes.form} onSubmit={handleSubmit}>
            <div>
              <h3>Name</h3>
              <Field
                className={classes.input}
                name="name"
                component="input"
                type="text"
                required
              />
            </div>
            <div>
              <h3>E-mail</h3>
              <Field
                className={classes.input}
                name="email"
                component="input"
                type="text"
                required
              />
            </div>
            <div>
              <h3>Age</h3>
              <Field
                className={classes.input}
                name="age"
                component="input"
                type="text"
                required
              />
            </div>
            <div className={classes.button}>
              <button className="button" type="submit" disabled={submitting}>
                Add
              </button>
            </div>
          </form>
        )}
      />
    </div>
  );
}

export default AddNote;
