import React from 'react';
import { createUseStyles } from 'react-jss';
import axios from 'axios';
import AddNote from './AddNote';
import NoteList from './NoteList';

const useStyles = createUseStyles({
  wrapper: {
    background: '#fff',
    padding: '50px 20px',
  },
  title: {
    color: '#000',
    fontSize: 28,
    fontWeight: 700,
    textAlign: 'center',
    marginBottom: 50,
  },
  inner: {
    display: 'flex',
    justifyContent: 'space-around',
  },
});

function DataTable() {
  const classes = useStyles();
  const [noteList, setNoteList] = React.useState(null);

  function addNote(data) {
    setNoteList([...noteList, { data }]);
  }

  function deleteNote(id) {
    axios.delete(`http://178.128.196.163:3000/api/records/${id}`)
      .then(setNoteList(noteList.filter((note) => note._id !== id)))
      .catch((err) => console.log(err));
  }

  const updateList = React.useCallback(() => {
    axios.get('http://178.128.196.163:3000/api/records')
      .then((res) => setNoteList(res.data))
      .catch((err) => console.log(err));
  }, []);

  React.useEffect(() => {
    updateList();
  }, [updateList]);

  return (
    <div className={classes.wrapper}>
      <h1 className={classes.title}>Таблица данных</h1>
      <div className={classes.inner}>
        <AddNote updateList={updateList} notes={noteList} addNote={addNote} />
        <NoteList data={noteList} deleteNote={deleteNote} />
      </div>
    </div>
  );
}

export default DataTable;
