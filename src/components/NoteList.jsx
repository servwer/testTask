import React from 'react';
import { createUseStyles } from 'react-jss';
import { Field, Form } from 'react-final-form';
import axios from 'axios';

const useStyles = createUseStyles({
  note_list: {
    width: 800,
    '& ul': {
      display: 'grid',
      gridTemplateColumns: '1fr 1fr 1fr 1fr',
      textAlign: 'left',
    },
  },
  button: {
    outline: 0,
    padding: 5,
    fontSize: 16,
    cursor: 'pointer',
  },
  input: {
    color: '#313131',
    padding: 5,
    marginTop: 10,
    '&::placeholder': {
      color: '#313131',
    },
  },
  danger: {
    color: '#4e4d4d',
    background: '#e25a5a87',
    border: '1px solid #bf434387',
    '&:hover': {
      background: '#ef131387',
      transform: 'scale(1.05)',
      color: '#000',
    },
  },
  item: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
});

function NoteList({ data, deleteNote }) {
  const classes = useStyles();

  const initialState = { _id: null, title: '', text: '' };
  const [current, setCurrent] = React.useState(initialState);
  function changeCurrentNote(item) {
    setCurrent(item);
  }

  function deleteCurrentNote(id) {
    const check = window.confirm('Вы хотите удалить запись?');
    if (check) deleteNote(id);
  }

  function onSubmit(value) {
    axios.post(`http://178.128.196.163:3000/api/records/${current._id}`, {
      data: {
        name: value[`name_${current._id}`],
        email: value[`email_${current._id}`],
        age: value[`age_${current._id}`],
        phone: value[`phone_${current._id}`],
      },
    })
      .then((res) => console.log(res))
      .catch((err) => console.log(err));
  }

  return (
    <div className={classes.note_list}>
      <h1>ТАБЛИЦА</h1>
      <ul>
        <li>age</li>
        <li>name</li>
        <li>email</li>
        <li>action</li>
      </ul>
      <Form
        onSubmit={onSubmit}
        validate={(values) => {
          const errors = {};
          if (!values[`age_${current._id}`]) {
            errors[`age_${current._id}`] = 'Required';
          }
          if (!values[`name_${current._id}`]) {
            errors[`name_${current._id}`] = 'Required';
          }
          if (!values[`email_${current._id}`]) {
            errors[`email_${current._id}`] = 'Required';
          }
          return errors;
        }}
        render={({
          handleSubmit, form, submitting, errors,
        }) => (
          <form onSubmit={handleSubmit}>
            {
              data ? data.map((item) => item.data && (
                <div key={`item_${item._id}`} className={classes.item}>
                  <Field
                    className={classes.input}
                    component="input"
                    disabled={item._id !== current._id}
                    type="text"
                    name={`age_${item._id}`}
                    defaultValue={item.data.age}
                    required
                  />
                  <Field
                    className={classes.input}
                    component="input"
                    disabled={item._id !== current._id}
                    type="text"
                    name={`name_${item._id}`}
                    defaultValue={item.data.name}
                    required
                  />
                  <Field
                    className={classes.input}
                    component="input"
                    disabled={item._id !== current._id}
                    type="text"
                    name={`email_${item._id}`}
                    defaultValue={item.data.email}
                    required
                    placeholder={item.data.email}
                  />
                  <button
                    type="button"
                    className={`${classes.button} ${classes.danger}`}
                    onClick={() => deleteCurrentNote(item._id)}
                  >
                    Удалить
                  </button>
                  {
                    item._id === current._id
                      ? (
                        <button
                          style={{ width: 124 }}
                          className={classes.button}
                          type="submit"
                          disabled={submitting}
                          onClick={() => {
                            setTimeout(() => {
                              changeCurrentNote(initialState);
                            }, 200);
                          }}
                        >
                          Сохранить
                        </button>
                      )
                      : (
                        <button
                          type="button"
                          style={{ width: 124 }}
                          className={classes.button}
                          onClick={() => {
                            changeCurrentNote(item);
                          }}
                        >
                          Редактировать
                        </button>
                      )
                  }
                </div>
              )) : null
            }
          </form>
        )}
      />
    </div>
  );
}

export default NoteList;
